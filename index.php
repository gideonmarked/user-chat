<!DOCTYPE html>
<html>
<head>
	<title>New Chat App</title>
	<style type="text/css">
		.load-chat {
		    background-color: #80adff;
		    display: block;
		    color: #000;
		}

		.load-chat p {
		    font-size: 10px;
		}

		div#chat-window {
		    background: antiquewhite;
		}
	</style>
</head>
<body>
	<div id="root"></div>
	<input type="hidden" id="username" value="<?php echo $_GET['u']; ?>" >
	<!-- Load Babel -->
	<script src="src/client/public/bundle.js"></script>
</body>
</html>