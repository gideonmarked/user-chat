<!DOCTYPE html>
<html>
<head>
	<title>New Chat App</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<style type="text/css">
		.load-chat {
		    background-color: #80adff;
		    display: block;
		    color: #000;
		}

		.load-chat p {
		    font-size: 10px;
		}

		div#chat-window {
		    background: #eaeaea;
	        padding: 20px;
		}

		.col-xs-12.nomapa.video-call {

		}

		.col-xs-5.messages-lkl a#showVideoCall {
		    display: inline-block;
		    width: 40px;
		    height: 40px;
		}

		.message {
		    background: #bdbdbd;
			min-width: 100px;
		    max-width: 500px;
		    width: 100px;
		    padding: 20px;
		    color: black;
		    font-weight: bold;
		    border-radius: 5px;
		    margin: 10px;
		}

		.message.from-me {
		    background: #54ff54;
		}

		.chat-content-wrap {
		    clear: both;
		    display: block;
		    width: 100%;
		    margin: 10px;
		    padding-top: 10px;
		}
	</style>
</head>
<body>
	<div id="root"></div>
	<input type="hidden" id="user" value="11682" >
	<!-- Load Babel -->
	<script src="../src/client/public/user_chat.js"></script>
</body>
</html>