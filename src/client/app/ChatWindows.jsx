import React from 'react';
import Peer from 'peerjs';

import ChatWindow from './ChatWindow.jsx';
import config from '../config/index.jsx';

class ChatWindows extends React.Component {
	constructor(props) {
	    super(props);

	    this.state = {
	    	owner: this.props.owner,
	    	socket: this.props.socket,
	    	selected_mate: this.props.selected_mate,
	    	messages: this.props.messages,
	    	peer: null,
	    	connect: null
	    };

	    this.addMessage = this.addMessage.bind(this);
	}

	componentWillReceiveProps(props) {

		//make sure socket is updated
		if( props.socket && this.state.socket == null ) {
			this.state.socket = props.socket;
		}

		//make sure to update incoming messages
		this.setState({
			messages: props.messages,
			owner: props.owner,
			selected_mate: props.selected_mate
		});



		if( this.state.peer == null ) {
			if( props.owner != null ) {
				this.setState({
					peer: new Peer(props.owner.id,{
				        host: config.peerurl,
				        port: config.peerport,
				        path: '/',
				        debug: 3,
				        secure: true,
				        config: {'iceServers': [
				        { url: 'stun:stun1.l.google.com:19302' },
				        { url: 'turn:numb.viagenie.ca',
				          credential: 'muazkh', username: 'webrtc@live.com' }
				        ]}
				    })
				});
			}
		}
    }

    addMessage( message ) {
    	this.props.addMessage( message );
    }

	render(){
		if( this.state.peer ) {
			var _this = this;

    		this.state.peer.on('open', function(){
		    });

		    this.state.peer.on('connection', function(connection){
			    _this.state.connect = connection;
			    var peer_id = connection.peer;
			});
    	}

		var chat_window = <div />;

		//process to configure windows
		if( this.state.messages ){
			var created_windows = [];
			var i = 0;

				chat_window = <ChatWindow
				key={ i++ }
				peer={ this.state.peer }
				connect={ this.state.connect }
				owner={ this.state.owner }
				mate={ this.state.selected_mate }
				socket={ this.state.socket }
				messages={ this.state.messages }
				addMessage={ this.addMessage }
				/>
		}
		
		return (
			<div className="chat-container-bottom">
            	{ chat_window }
          	</div>
		);
	}
}

export default ChatWindows;