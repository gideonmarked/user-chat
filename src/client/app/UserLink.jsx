import React from 'react';

class UserLink extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			user: this.props.user
		};
		this.clickHandler = this.clickHandler.bind(this);
	}

	clickHandler(event) {

	    // Call the onSend callback with the chatInput message
	    this.props.onClickUpdate(this.state.user);
	    
	    // Stop the form from refreshing the page on submit
	    event.preventDefault();
	}

	render() {
		var img = '';

		if( this.state.user.image == null || this.state.user.image == 'null' || this.state.user.image == '' ) {
			if( this.state.user.sex == 'male' ) {
				img = 'http://www.betset.io/public//uploads/users/profile_m.png';
			} else {
				img = 'http://www.betset.io/public//uploads/users/profile_f.png';
			}
		} else {
			img = 'http://www.betset.io/public//uploads/users/' + this.state.user.image;
		}

		return (
			<a href="#" onClick={ this.clickHandler } className="load-chat clearfix active-chat" data-userid="11462" id="chat-open-11462" data-isonline="0">
	            <div className="messeges-image">
	                <img src={ img } alt="" width={ 50 } />
	            </div>
	            <div className="mess-top">
	                <div>
	                    <span className="status online-status0"></span>
	                    <span className="name">{ this.state.user.nickname }</span>
	                </div>
	                <p>{ this.state.user.bio }</p>
	                <time>26.10.2017, 19:23</time>
	            </div>
	        </a>
		);
	}
}

export default UserLink;