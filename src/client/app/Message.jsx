import React from 'react';

class Message extends React.Component {
  render() {
    // Was the message sent by the current user. If so, add a css class
    const fromMe = this.props.message.fromMe ? 'from-me' : '';
    var img = '';


    if( this.props.message.sender.image == null || this.props.message.sender.image == 'null' || this.props.message.sender.image == '' ) {
        if( this.props.message.sender.sex == 'male' ) {
            img = 'http://www.betset.io/public//uploads/users/profile_m.png';
        } else {
            img = 'http://www.betset.io/public//uploads/users/profile_f.png';
        }
    } else {
        img = 'http://www.betset.io/public//uploads/users/' + this.props.message.sender.image;
    }

    return (
      <div className={`chat-message ${fromMe}`}>
        <time></time>
        <img src={ img } width={ 50 } />
        <p>
          { this.props.message.message }
        </p>
      </div>
    );
  }
}

Message.defaultProps = {
  message: '',
  fromMe: false
};

export default Message;