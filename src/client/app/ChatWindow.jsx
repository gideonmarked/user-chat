import React from 'react';
import config from '../config/index.jsx';

import Messages from './Messages.jsx';
import ChatInput from './ChatInput.jsx';

class ChatWindow extends React.Component {
	constructor(props) {
	    super(props);
	    this.state = {
	    	messages: this.props.messages,
	    	owner: this.props.owner,
	    	mate: this.props.mate,
            socket: this.props.socket,
            show_video: false,
            peer: this.props.peer,
            connect: this.props.connect,
            call: null
	    };
        this.sendHandler = this.sendHandler.bind(this);
        this.toggleVideo = this.toggleVideo.bind(this);
	    this.onReceiveStream = this.onReceiveStream.bind(this);

        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
	}

    componentWillReceiveProps(props) {
        this.setState({
            messages: props.messages,
            mate: props.mate
        });

        if( props.messages[ props.messages.length -1 ].type == 'video' ) {
            var xhr = new XMLHttpRequest();
            xhr.responseType = 'blob';

            xhr.onload = function() {
    
                var reader = new FileReader();
                
                reader.onloadend = function() {
                
                var byteCharacters = atob(reader.result.slice(reader.result.indexOf(',') + 1));

                var byteNumbers = new Array(byteCharacters.length);

                for (var i = 0; i < byteCharacters.length; i++) {

                byteNumbers[i] = byteCharacters.charCodeAt(i);

                }

                var byteArray = new Uint8Array(byteNumbers);
                var blob = new Blob([byteArray], {type: 'video/MP4'});
                var url = URL.createObjectURL(blob);

                document.getElementById('peer-camera').src = url;
              
            }
            
            reader.readAsDataURL(xhr.response);
            
          };

          xhr.open('GET', props.messages[ props.messages.length -1 ].url);
          xhr.send();
        }
    }

	sendHandler(message) {
	    const messageObject = {
	      sender: this.state.owner,
	      receiver: this.state.mate,
	      message
	    };

        messageObject.timestamp = Date.now();
        messageObject.fromMe = false;

        // Emit the message to the server
        this.state.socket.emit('client:message', messageObject);

        messageObject.fromMe = true;

	    //sends messages to some user
	    this.props.addMessage(messageObject);
        this.getVideo = this.getVideo.bind(this);
	}

	addMessage(message) {
	    // Append the message to the component state
	    const messages = this.state.messages;
	    messages.push(message);
	    this.setState({ messages });
	}

    toggleVideo(event) {

        var _this = this;

        this.setState({
            show_video: !this.state.show_video
        });

        if(!this.state.show_video) {
            this.getVideo(function(stream){
                window.localStream = stream;
                _this.onReceiveStream(stream, 'my-camera');
                var call = _this.state.peer.call(_this.state.mate.id, window.localStream);
            });
        }

        event.preventDefault();
    }

    getVideo(callback){
        var _this = this;

        navigator.getUserMedia({audio: false, video: true}, callback, function(error){
          alert('An error occured. Please try again');
        });
    }

    onReceiveStream(stream, element_id){
        var video = document.getElementById(element_id).getElementsByTagName('video')[0];
        video.src = window.URL.createObjectURL(stream);
        window.peer_stream = stream;
    }


	render() {

        var video_streaming = null;

        if( this.state.show_video ) {
            video_streaming = (
                <div className="messages-video-call">
                    <div className="col-xs-12 nomapa video-call">
                        <div className="col-xs-6 nomapa">
                            <div id="my-camera" className="camera">
                                <video width="365" muted height="220" autoPlay></video>
                            </div>
                        </div>
                        <div className="col-xs-6 nomapa">
                            <div className="camera">
                                <video width="365" muted height="220" id="peer-camera" autoPlay></video>
                            </div>
                        </div>
                    </div>
                    <div className="col-xs-12 messages-video-line nomapa">
                        <a href="#">
                            <img src="http://www.betset.io/img/messages-video-end.png" alt="" className="img-responsive messages-video-end pull-left" data-peer="11462" data-peername="Legbcc63" id="top-video-end" />
                        </a>
                        <div className="messages-video-menu pull-left">
                            <img src="http://www.betset.io/img/messages-video-vid.png" alt="" className="pull-left messages-video-vid" />
                            <img src="http://www.betset.io/img/messages-video-mic.png" alt="" className="pull-left messages-video-mic" />
                            <a href="#">
                                <img src="http://www.betset.io/img/messages-video-menu.png" alt="" className="pull-left messages-video-menu2" />
                            </a>
                        </div>
                    </div>
                </div>
            );
        }

        var img = '';

        if( this.state.mate.image == null || this.state.mate.image == 'null' || this.state.mate.image == '' ) {
            if( this.state.mate.sex == 'male' ) {
                img = 'http://www.betset.io/public//uploads/users/profile_m.png';
            } else {
                img = 'http://www.betset.io/public//uploads/users/profile_f.png';
            }
        } else {
            img = 'http://www.betset.io/public//uploads/users/' + this.state.mate.image;
        }

	    return (
			<div className="chat-container" id="chat-window">
               	<div id="chat-top-right">
                    <div className="row messeges-myprofile">
                        <div className="col-xs-6 messeges-myprofile-img">
                            <a href={"http://www.betset.io/user/" + this.state.mate.id} className="profile-pic" id="chat-top-id">
                                <img src={ img } alt="" id="chat-top-id-img" width={ 50 } />
                            </a>
                            <div className="user-other">
                                <div className="nickname">
                                    <span className="online-status" id="chat-top-status"></span>
                                    <span id="chat-user">{ this.state.mate.nickname }</span>
                                </div>
                                <div className="location" id="chat-location">{ this.state.mate.address }</div>
                                <div className="age" id="chat-age">{ this.state.mate.age } years</div>
                            </div>
                        </div>
                        <div className="col-xs-5 messages-lkl">
                            <a href="#" onClick={ this.toggleVideo } id="showVideoCall" data-toggle="tooltip" data-placement="bottom" title="You must upgrade to PREMIUM  membership to be able to use this feature." data-original-title="">
                                <i id="showVideoCallIcon" className="ico ico-video 0"></i>
                            </a>
                            <a className="chat-star" data-userid="11462" id="action-profile-favorite">
                                <i className="fa fa-star"></i>
                            </a>
                            <div className="messages-menu-hov" id="messages-menu-hov">
                                <i className="fa fa-bars"></i>
                                <div id="messages-conv-menu" className="messages-conv-menu">
                                    <a href="#" id="delete-messages" data-userid="11462">Delete Conversation</a>
                                    <a href="http://www.betset.io/user/11462" id="top-view-profile">View Profile</a>
                                    <a href="#" id="top-block-unblock" data-userid="11462" data-status="unblocked">Block Contact</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    { video_streaming }
                </div>
                <div className="chat-content-wrap withChat mCustomScrollbar _mCS_1 mCS_no_scrollbar" id="chat-content-wrap">
                    <Messages messages={ this.state.messages } mate={ this.state.mate } />
                    <div className="messages-video-bor">
                        <ChatInput onSend={ this.sendHandler } />
                        <div className="chat-bottom">
                            <a href="#" className="sendmessage" id="sendMessage">
                                <div>SEND</div>
                                <img src="http://www.betset.io/img/massages-arrow.png" alt="" />
                            </a>
                            <a href="#" className="mess-lk2" id="toggleChatGifts">
                                <i className="ico ico-gift"></i>
                            </a>
                            <a href="#" className="mess-lk" id="toggleEmojiMenu">
                                <i className="ico ico-emoji"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
	    );
	}
}

ChatWindow.defaultProps = {
	  username: 'Anonymous',
	  recepient: 'Anonymous'
};

export default ChatWindow;