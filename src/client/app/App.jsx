import React from 'react';
import ReactDOM from 'react-dom';
import ChatApp from './ChatApp.jsx';

navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: document.getElementById('user').value
    };
  }

  render() {
      return (
        <ChatApp user={ this.state.user }/>
      );
  }

}
App.defaultProps = {
};

ReactDOM.render(<App/>, document.getElementById('root'));