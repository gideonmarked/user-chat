import React from 'react';

import Message from './Message.jsx';

class Messages extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mate: this.props.mate,
      messages: this.props.messages
    };
  }

  componentDidUpdate() {
    // There is a new message in the state, scroll to bottom of list
    const objDiv = document.getElementById('messageList');
    objDiv.scrollTop = objDiv.scrollHeight;
  }

  componentWillReceiveProps(props) {
    if( props.messages ) {
      this.setState({
        messages: props.messages,
        mate: props.mate
      });
    }
  }

  render() {
    // Loop through all the messages and filter
    var messages = null;
    
    if( this.state.messages && this.state.messages.length > 0 ) {
      messages = this.state.messages.map((message, i) => {

        if( message.receiver.email == this.state.mate.email ){
          message.fromMe = true;
        }

        if( message.fromMe
          ||
          (this.state.mate.email == message.sender.email) && message.type != 'video' ){
          return (
            <Message
              key={i}
              message={message}
               />
          );
        }
      });
    } else {
      messages = <div />;
    }
    return (
      <div className='messages' id='messageList' style={{ height: '300px' }}>
        { messages }
      </div>
    );
  }
}

Messages.defaultProps = {
  messages: [],
  owner: 'Anonymous'
};

export default Messages;