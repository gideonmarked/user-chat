import React from 'react';
import io from 'socket.io-client';
import config from '../config/index.jsx';

import Messages from './Messages.jsx';
import UserList from './UserList.jsx';
import ChatWindows from './ChatWindows.jsx';

class ChatApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],

      messages: null,

      current_mate: null,

      user: this.props.user,

      socket: null,

      owner: null
    };

    var _this = this;

    this.updateUser = this.updateUser.bind(this);
    this.addMessage = this.addMessage.bind(this);
    this.addSelfMessage = this.addSelfMessage.bind(this);
    this.addSavedMessage = this.addSavedMessage.bind(this);


    if(this.state.user) {
      fetch(config.chat_converter + this.state.user)
      .then( results => {
        return results.json();
      })
      .then( data => {
        this.setState({owner: data});
        const username = data.email
        this.state.socket = io(config.server, { query: `username=${username}`, secure: true }).connect();
        this.state.socket.emit('client:join', { username: username });
        this.state.socket.on('server:join_success', msg => {
        });

        this.state.socket.on('server:message', message => {
          //this should add message when 
          this.addMessage(message);
        });

        this.state.socket.on('server:saved_message', message => {
          //this should add message when
          console.log('saved message', message);
          this.addSavedMessage(message);
        });

      });
    }
  }

  componentWillReceiveProps(props) {

  }

  componentDidMount() {
    
  }

  addMessage(message) {
      // Append the message to the component state
      const active = true;
      //filter messages for this user only
      if( message.receiver.email == this.state.owner.email ) {
        //this should set the message
        // check if there's no existing users list
        if( this.state.users.length == 0 ) {
          this.setState({
            users: [ message.sender ],
            messages: [ message ],
            selected_mate: message.sender
          });
          //goes here if there's existing users
        } else {
          var match = false;
          //check if the sender exists as user already
          this.state.users.map( function(user,i) {
            if( message.sender.email == user.email )
            {
              match = true;
            }
          });
          
          //if match is not found add the user to the users list
          if( !match ) {
            this.setState({
              users: [...this.state.users, message.sender],
              messages: [...this.state.messages,message]
            });
          } else {
            this.setState({
              messages: [...this.state.messages,message]
            });
          }
        }
      }
  }

  addSavedMessage(message) {
      // Append the message to the component state
      const active = true;
      //filter messages for this user only
      if( message.receiver.email == this.state.owner.email ) {
        //this should set the message
        // check if there's no existing users list
        if( this.state.users.length == 0 ) {
          this.setState({
            users: [ message.sender ],
            messages: [ message ],
            selected_mate: message.sender
          });
          //goes here if there's existing users
        } else {
          var match = false;
          //check if the sender exists as user already
          this.state.users.map( function(user,i) {
            if( message.sender.email == user.email )
            {
              match = true;
            }
          });
          
          //if match is not found add the user to the users list
          if( !match ) {
            this.setState({
              users: [...this.state.users, message.sender],
              messages: [...this.state.messages,message]
            });
          } else {
            this.setState({
              messages: [...this.state.messages,message]
            });
          }
        }
      } else {
        console.log('shoud add my message');
        this.addSelfMessage( message );
      }
  }

  updateUser( user ) {
    this.setState({ selected_mate: user});
  }

  addSelfMessage( message ) {
    this.setState({
      messages: [...this.state.messages,message]
    })
  }

  render() {
    var userlist = null;
    if( this.state.users.length == 0 ) {
      userlist = <div> No Users. </div>
    } else {
      userlist = <UserList users={ this.state.users } onUpdateReceiver={ this.updateUser } />;
    }

    return (
      <div className="row messages-body">
        <div className="col-sm-4 col-xs-12" id="messages-left">
          <div className="messeges-bord">
            <a href="#" id="showOnlineOnly" className="show-online-only">Only online users <i className="ico ico-onlyonline"></i></a>
            <h2>Messages</h2>
            <input placeholder="Search People" id="search-messages" className="mess-inp" />
            <div className="chats-list" id="users-list">
              { userlist }
            </div>
          </div>
        </div>
        <div className="col-sm-8 col-xs-12">
          <ChatWindows
          messages={ this.state.messages }
          owner={ this.state.owner }
          socket={ this.state.socket }
          selected_mate={ this.state.selected_mate }
          addMessage={ this.addSelfMessage }
          peer={ this.state.peer }
          />
        </div>
      </div>
    );
  }

}

ChatApp.defaultProps = {
  username: 'Anonymous'
};

export default ChatApp;