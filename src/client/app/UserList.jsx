import React from 'react';

import UserLink from './UserLink.jsx';

class UserList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			users: this.props.users,
			owner: this.props.owner
		};
		this.updateClickHandler = this.updateClickHandler.bind(this);
	}

	componentWillReceiveProps( props ) {
		this.setState({
			users: props.users
		});
	}

	updateClickHandler( user )
	{
		this.props.onUpdateReceiver( user );
	}

	render() {
		if ( !this.state.users ) {
			return ( <div /> );
		}

		const users = this.props.users.map((user, i) => {
			return (
				<UserLink
				key={ i }
				user={ user }
				onClickUpdate={ this.updateClickHandler }
				 />
			);
	    });

	    return (
	      <div className='messages' id='messageList'>
	        { users }
	      </div>
	    );
	}
}

export default UserList;